import com.sdiemert.jgt.core.*;

import java.util.ArrayList;

public class SimpleDeleteExample {

    /**
     * In this example a simple triangle host graph is created.
     *
     * The rule graph matches a edge of the triangle and then deletes that edge.
     *
     * The expected result is a graph with a single node since delete two nodes in the
     * triangle requires that all three edges be deleted (as per the SPO GT approach).
     */
    public static void run() throws GraphException{

        // STEP 1) Create the host graph.

        Graph h = new Graph();

        Node nh0 = new Node("nh0", "A");
        Node nh1 = new Node("nh1", "A");
        Node nh2 = new Node("nh2","A");

        Edge eh0 = new Edge("eh0", nh0, nh1, "e");
        Edge eh1 = new Edge("eh1", nh1, nh2, "e");
        Edge eh2 = new Edge("eh2", nh2, nh0, "e");

        h.addNodes(nh0, nh1, nh2);
        h.addEdges(eh0, eh1, eh2);

        // STEP 2) Create the rule

        Graph g = new Graph();

        Node ng0 = new Node("n0", "A");
        Node ng1 = new Node("n1", "A");
        Edge eg0 = new Edge("e0", ng0, ng1, "e");

        g.addNodes(ng0, ng1);
        g.addEdges(eg0);

        // denote the nodes and edges that we are adding using this rule.
        ArrayList<Node> delNodes = new ArrayList<Node>();
        ArrayList<Edge> delEdges = new ArrayList<Edge>();
        delNodes.add(ng0);
        delNodes.add(ng1);
        delEdges.add(eg0);

        Rule r = new Rule(g, null, null, delNodes, delEdges, null);

        // STEP 3) Apply the Rule r to the host graph h (printing before and after to see the difference).

        System.out.println("Before:");
        System.out.println(h);
        System.out.println("-------------------");
        System.out.println("Calling r.apply(h).");
        boolean result = r.apply(h);
        System.out.println("r.apply(h) completed with: " + result);
        System.out.println("-------------------");
        System.out.println("After:");
        System.out.println(h);

    }

}
