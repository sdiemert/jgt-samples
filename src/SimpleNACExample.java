import com.sdiemert.jgt.core.*;

import java.util.ArrayList;

public class SimpleNACExample {

    /**
     * In this example a simple triangle host graph is created.
     *
     * A rule with a Negative Application Condition (NAC) is created. The NAC
     * means that the rule will only be applied if the graph condition specified
     * by the NAC is *not* satisfied.
     */
    public static void run() throws GraphException, RuleException{

        // STEP 1) Create the host graph.

        Graph h = new Graph();

        Node nh0 = new Node("nh0", "A");
        Node nh1 = new Node("nh1", "A");
        Node nh2 = new Node("nh2","A");

        Edge eh0 = new Edge("eh0", nh0, nh1, "e");
        Edge eh1 = new Edge("eh1", nh1, nh2, "e");
        Edge eh2 = new Edge("eh2", nh2, nh0, "e");

        h.addNodes(nh0, nh1, nh2);
        h.addEdges(eh0, eh1, eh2);

        // STEP 2) Create the rule and the NAC

        Graph g = new Graph();

        Node ng0 = new Node("n0", "A");
        Node ng1 = new Node("n1", "A");
        Node ng2 = new Node("n2", "B");

        Edge eg0 = new Edge("e0", ng0, ng1, "e");
        Edge eg1 = new Edge("e1", ng0, ng2, "E");
        g.addNodes(ng0, ng1, ng2);

        // denote the nodes and edges that we are adding using this rule.
        ArrayList<Node> addNodes = new ArrayList<Node>();
        ArrayList<Edge> addEdges = new ArrayList<Edge>();
        addNodes.add(ng2);
        addEdges.add(eg1);

        // STEP 2.1)  Create the NAC
        // we will only match graphs if there is NOT an A-e->A edge in
        // the matching graph.
        ArrayList<NAC> nacs = new ArrayList<NAC>();
        nacs.add(new NAC(eg0));

        // denote the nodes and edges that we are adding using this rule.
        Rule r = new Rule(g, addNodes, addEdges, null, null, nacs);

        // STEP 3) Apply the Rule r to the host graph h (printing before and after to see the difference).

        System.out.println("Before:");
        System.out.println(h);
        System.out.println("-------------------");
        System.out.println("Calling r.apply(h).");
        boolean result = r.apply(h);
        System.out.println("r.apply(h) completed with: " + result);
        System.out.println("-------------------");
        System.out.println("After:");
        System.out.println(h);

    }

}
