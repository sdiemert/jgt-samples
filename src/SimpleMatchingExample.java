import com.sdiemert.jgt.core.*;

import java.util.ArrayList;

public class SimpleMatchingExample {

    /**
     * In this example a simple triangle host graph is created.
     *
     * The rule graph matches a edge of the triangle and then adds another node with
     * an additional edge.
     */
    public static void run() throws GraphException{

        // STEP 1) Create the host graph.

        Graph h = new Graph();

        Node nh0 = new Node("nh0", "A");
        Node nh1 = new Node("nh1", "A");
        Node nh2 = new Node("nh2","A");

        Edge eh0 = new Edge("eh0", nh0, nh1, "e");
        Edge eh1 = new Edge("eh1", nh1, nh2, "e");
        Edge eh2 = new Edge("eh2", nh2, nh0, "e");

        h.addNodes(nh0, nh1, nh2);
        h.addEdges(eh0, eh1, eh2);

        // STEP 2) Create the rule

        Graph g = new Graph();

        Node ng0 = new Node("n0", "A");
        Node ng1 = new Node("n1", "A");
        Node ng2 = new Node("n2", "B");

        Edge eg0 = new Edge("e0", ng0, ng1, "e");
        Edge eg1 = new Edge("e1", ng0, ng2, "E");
        Edge eg2 = new Edge("e2", ng1, ng2, "E");

        g.addNodes(ng0, ng1, ng2);
        g.addEdges(eg0, eg1, eg2);

        // denote the nodes and edges that we are adding using this rule.
        ArrayList<Node> addNodes = new ArrayList<Node>();
        ArrayList<Edge> addEdges = new ArrayList<Edge>();
        addNodes.add(ng2);
        addEdges.add(eg1);
        addEdges.add(eg2);

        Rule r = new Rule(g, addNodes, addEdges, null, null, null);

        // STEP 3) Apply the Rule r to the host graph h (printing before and after to see the difference).

        System.out.println("Before:");
        System.out.println(h);
        System.out.println("-------------------");
        System.out.println("Calling r.apply(h).");
        boolean result = r.apply(h);
        System.out.println("r.apply(h) completed with: " + result);
        System.out.println("-------------------");
        System.out.println("After:");
        System.out.println(h);

    }

}
