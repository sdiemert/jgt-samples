
public class Main {

    public static void main(String[] args) throws Exception
    {

        System.out.println("=======================");
        SimpleMatchingExample.run();
        System.out.println("=======================");
        SimpleDeleteExample.run();
        System.out.println("=======================");
        SimpleNACExample.run();
        System.out.println("=======================");
        matchingDataElementsExample.run();
        System.out.println("=======================");

    }
}
