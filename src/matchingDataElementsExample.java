import com.sdiemert.jgt.core.*;

import java.util.ArrayList;

public class matchingDataElementsExample {

    /**
     * In this example a simple triangle host graph is created containing additional data
     * elements that should be matched against.
     *
     */
    public static void run() throws GraphException, RuleException{

        // STEP 1) Create the host graph.

        Graph h = new Graph();

        Node nh0 = new Node<StringNodeData>("nh0", "A", new StringNodeData("hello"));
        Node nh1 = new Node<IntNodeData>("nh1", "A", new IntNodeData(1));
        Node nh2 = new Node("nh2","B");

        Edge eh0 = new Edge("eh0", nh0, nh1, "e");
        Edge eh1 = new Edge("eh1", nh1, nh2, "e");
        Edge eh2 = new Edge("eh2", nh2, nh0, "e");

        h.addNodes(nh0, nh1, nh2);
        h.addEdges(eh0, eh1, eh2);

        // STEP 2) Create the rule and the NAC

        Graph g = new Graph();

        // This node will match the nh0 directly.
        Node ng0 = new Node<StringNodeData>("n0", "A", new StringNodeData("hello"));

        // This node will match to nh1 even though it does not have a Integer data.
        Node ng1 = new Node("n1", "A");

        Node ng2 = new Node("n2", "B");

        Edge eg0 = new Edge("e0", ng0, ng1, "e");
        Edge eg1 = new Edge("e1", ng0, ng2, "E");
        g.addNodes(ng0, ng1, ng2);

        // denote the nodes and edges that we are adding using this rule.
        ArrayList<Node> addNodes = new ArrayList<Node>();
        ArrayList<Edge> addEdges = new ArrayList<Edge>();
        addNodes.add(ng2);
        addEdges.add(eg1);

        // denote the nodes and edges that we are adding using this rule.
        Rule r = new Rule(g, addNodes, addEdges, null, null, null);

        // STEP 3) Apply the Rule r to the host graph h (printing before and after to see the difference).

        System.out.println("Before:");
        System.out.println(h);
        System.out.println("-------------------");
        System.out.println("Calling r.apply(h).");
        boolean result = r.apply(h);
        System.out.println("r.apply(h) completed with: " + result);
        System.out.println("-------------------");
        System.out.println("After:");
        System.out.println(h);

    }

}
