# jGT Samples

This repository contains examples demonstrating how to use the [jGT](https://gitlab.com/sdiemert/jgt) API to execute graph transformations.